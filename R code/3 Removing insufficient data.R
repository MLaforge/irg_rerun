#########################################################################################################

####################  Subsetting data for approporiate number of locs ###################################

#########################################################################################################


### Read in and merge the data

data<-read.csv("Outputs/SeasonSubsetted/CaribouSpring.csv")
library(data.table)
head(data)

data$AnimID<-data$ANIMAL_ID
data$IDYear<-paste(data$AnimID,data$Year, sep="")

# Determine the number of locs by animal*block

NewD<-data.table(data) ## Make it a data table

NewD[, Counts := .N, by = .(IDYear)] ## Column representing the number of locations per ID*Year

NewD$PtsPerDay<-NewD$Counts/NewD$SLength ## Mean number of Locs per day for each ID*Year

## Some summary stats
median(NewD$PtsPerDay)
range(NewD$PtsPerDay)

NewD[, dayCount := length(EASTING), by = .(IDYear, JDate)]## Number of fixes per day (necessary??)

length(unique(NewD$IDYear))


i<-4 ## Threshold value for minimum number of mean fixes per day to retain 
j<-0.9 ### Threshold value for proportion of days that must have at least one fix to retain


NewD2<-NewD[PtsPerDay > i]# Subset to only include individuals with a mean of at least i points per day
length(unique(NewD2$IDYear))  ## Down to 367 ID*Year combinations
NewD2$IDYearDay<-paste(NewD2$IDYear,NewD2$JDate, sep="") ## Make a new Year*ID*Julian Day column
NewD2[, datDay := length(unique(IDYearDay)), by = .(IDYear)] ## Calculate the number of days in which there is data for each Year*ID
NewD2$PropDataDays<-NewD2$datDay/(trunc(NewD2$SLength)+1) ## Calculate the proportion of days in which there is data for each Year*ID
DataEdited<-NewD2[PropDataDays>=j] ## Remove all ID*Year combos for which the proportion of days with at least one fix is < j
(1-(length(unique(NewD$IDYear))-length(unique(DataEdited$IDYear)))/length(unique(NewD$IDYear)))*100 ##Percent data retained 

## This retains 90.3%

write.csv(DataEdited,"Outputs/DataNew.csv")





