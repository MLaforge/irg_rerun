################################################################################

##############    Calculating summary data tables for Results   ################

################################################################################


BeforeIns<-read.csv("Outputs/SeasonSubsetted/CaribouSpring.csv")


head(BeforeIns)

nrow(BeforeIns) # 380,936

unique(BeforeIns$ANIMAL_ID) #128
unique(BeforeIns$burst)  #411

AfterClean<-read.csv("Outputs/DataNew.csv")

nrow(AfterClean)
unique(AfterClean$ANIMAL_ID) #125
unique(AfterClean$burst)  #371

nrow(AfterClean)/length(unique(AfterClean$burst)) ## mean fixes: 984.4

library(data.table)

DT<-data.table(AfterClean)

## Summary by herd*year

DT[,.(.N),by = .(year,HERD)]

## Summary by herd

DT[,.(.N),by = .(HERD)]

## summary by year

DT[,.(.N),by = .(year)]

## Individuals by HERD * year

DT[,length(unique(ANIMAL_ID)),by = .(year,HERD)]

## Individuals by HERD

DT[,length(unique(burst)),by = .(HERD)]

## Individuals by year

DT[,length(unique(burst)),by = .(year)]

head(DT)


########  Green up stats:

DT[,mean(MedGreen),by = .(year,HERD)]

Buch<-read.csv("Outputs/GreenUpStats/BuchansGreenStats.csv")

Buch





